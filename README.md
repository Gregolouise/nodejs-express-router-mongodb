# README #

Correction de l'exercice visant à implementer Mongodb dans l'application NodeJs

### What was asked ###

1. En suivant les explications des précédents slides, créer un compte MongoDb Atlas gratuit

2. A partir du Serveur NodeJs / Express construit lors du précedent exercice, l’objectif est de créer un CRUD vers une table MongoDb

3. Installation de Mongoose (https://www.npmjs.com/package/mongoose)

4. Avec l’aide de la documentation Mongoose (https://mongoosejs.com/docs/middleware.html), modifier le fichier Orders.js pour créer un CRUD comme suivant :
 
	GET /orders (voir Model.find() )

	GET /orders/id (voir Model.findById() )
 	
	POST /orders (voir Model.save() )
 	
	UPDATE /orders/id (voir Model.updateOne() ) 

	DELETE /orders/id (voir Model.deleteOne() )

### Who do I talk to? ###

* gregoire.louise@lapilulerouge.io